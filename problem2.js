
const fs =require('fs')
function fsCallback(filename){
fs.readFile(`${__dirname}/${filename}`,'utf8', (err,data)=>{
    if (err){
        throw err
    }else{
        let upperCaseData = data.toUpperCase();
        fs.writeFile(`${__dirname}/upperCase.txt`,upperCaseData,(err,upperCaseFile)=>{
            if (err) {
                throw err;
            }else{
                // console.log(upperCaseData)
                fs.appendFile(`${__dirname}/filenames.txt`, 'upperCase.txt ',(err, names)=>{
                    if (err){
                        throw err;
                    }else{
                        console.log('upperCase.txt is added to filenames.txt')
                    }
                })
                fs.readFile(`${__dirname}/upperCase.txt`,'utf8', (err,upperCase)=>{
                    if (err){
                        throw err;
                    }else{
                        let lowerCase = upperCase.toLowerCase().split('.');
                        let flag = 0;
                        for (let i = 0; i < lowerCase.length; i++){
                                fs.appendFile(`${__dirname}/lowerCase.txt`, lowerCase[i]+".\n", (err,lowerCaseFile)=>{
                                    if (err){
                                        throw err;
                                    }else{
                                        flag += 1;
                                    }
                                    // console.log(flag,i)
                                    if (flag == lowerCase.length){
                                        fs.appendFile(`${__dirname}/filenames.txt`, 'lowerCase.txt ',(err, names)=>{
                                            if (err){
                                                throw err;
                                            }else{
                                                console.log('lowerCase.txt is added to filenames.txt')
                                            }
                                        })
                                        fs.readFile(`${__dirname}/lowerCase.txt`, 'utf8', (err, lowerCaseFile)=>{
                                            if (err){
                                                throw err;
                                            }else{
                                                let array =lowerCaseFile.split('.\n');
                                                let sortedString = array.sort();
                                                // console.log(sortedString)
                                                let count=0;
                                                sortedString.forEach(element => {
                                                    fs.appendFile(`${__dirname}/sortedFile.txt`, element+".\n", (err, sortedFile)=>{
                                                        if (err){
                                                            throw err;
                                                        }else{
                                                            count += 1;
                                                        }
                                                        if (count == sortedString.length){
                                                            fs.appendFile(`${__dirname}/filenames.txt`, 'sortedFile.txt',(err, names)=>{
                                                                if (err){
                                                                    throw err;
                                                                }else{
                                                                    console.log('sortedFile.txt is added tofilenames.txt')
                                                                    fs.readFile(`${__dirname}/filenames.txt`, 'utf8', (err, files)=>{
                                                                        if (err){
                                                                            throw err;
                                                                        }else{
                                                                            let fileName = files.split(' ');
                                                                            let count = 0;
                                                                            let duplicate = fileName;
                                                                            fileName.forEach((element)=>{
                                                                                if (duplicate.length !=0  && element !== ' '){
                                                                                    fs.unlink(`${__dirname}/${element}`, (err) => {
                                                                                        if (err) {
                                                                                            throw err;
                                                                                        }else{
                                                                                            count+=1
                                                                                            delete duplicate[fileName.indexOf(element)]
                                                                                            if (count == fileName.length){
                                                                                                fs.writeFile(`${__dirname}/filenames.txt`, '',(err, newFile)=>{
                                                                                                    if (err){
                                                                                                        throw err;
                                                                                                    }else{
                                                                                                        console.log('content of filename is cleared')
                                                                                                    }
                                                                                                } )
                                                                                            }
                                                                                        }
                                                                                    });
                                                                                }
                                                                            })
                                                                        }
                                                                    })
                                                                }
                                                            })
                                                        }
                                                    })
                                                });
                                            }
                                        })
                                    }
                                })
                        }
                    }
                })
            }
        })
    }
})
}
module.exports = {fsCallback};