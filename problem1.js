
const fs =require('fs')
// import { unlink } from 'fs';
function JSONFile(fileName,data){
    fs.writeFile(`${__dirname}/${fileName}.json`,JSON.stringify(data),function(err){
        if (err){
        throw err;
        }else{
            console.log(`file successfully created ${fileName}`);
        }
    })

    fs.unlink(`${__dirname}/${fileName}.json`, (err) => {
    if (err) {
        throw err;
    }else{
        console.log(`successfully deleted ${fileName}`);
    }
    });
}
module.exports = {JSONFile};